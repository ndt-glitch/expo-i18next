import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'
import * as Localization from 'expo-localization'

import en_simple from './en/simple.json'
import en_complex from './en/complex.json'
import ja_simple from './ja/simple.json'
import ja_complex from './ja/complex.json'
import ru_simple from './ru/simple.json'
import ru_complex from './ru/complex.json'
import vi_simple from './vi/simple.json'
import vi_complex from './vi/complex.json'

const languageDetector = {
    type: 'languageDetector',
    async: true,
    detect: (cb) => cb(Localization.locale.split('-')[0]),
    init: () => { },
    cacheUserLanguage: () => { }
}

const options = {
    debug: true,
    resources: {
        en: { simple: en_simple, complex: en_complex },
        ja: { simple: ja_simple, complex: ja_complex },
        ru: { simple: ru_simple, complex: ru_complex },
        vi: { simple: vi_simple, complex: vi_complex }
    },
    lng: 'en',
    fallbackLng: 'en',
    defaultNS: 'common',
    ns: ['simple', 'complex'],
    saveMissing: true
}

const extractTranslation = {
    type: 'backend',
    addPath: lang => `./${lang}/${ns}/`,
    create: function (langs, ns, key, fallbackValue) {
        /* save the missing translation */
        if (typeof langs === 'string') langs = [langs]
        const translation = { [key]: fallbackValue || 'empty_string' }
        langs.forEach(lang => {
            // fs.writeFile(this.addPath(lang), JSON.stringify(translation))
            // Find away in Expo to write this down to the repo
            // TODO I should check on how many way <Trans> could work and apply it for shootismoke now
        });
    }
}

i18next
    .use(languageDetector)
    .use(initReactI18next)
    .use(extractTranslation)
    .init(options)

export default i18next