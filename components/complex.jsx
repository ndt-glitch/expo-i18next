import React from 'react'
import { ScrollView } from 'react-native'
import { useTranslation, Trans } from 'react-i18next'
import { Txt, Br } from './mini'


export default function (props) {
    const { t } = useTranslation('complex')


    /**
     * @i18next_parser
     * 
     * > dynamic_script_${random}
     * t('dynamic_script_1')
     * t('dynamic_script_2')
     * t('dynamic_script_3')
     * 
     * > context_list
     * t('context_list')
     * t('context_list_2')
     * t('context_list_3')
     * 
     * > nesting_trans
     * t('nesting_key')
     */
    const max = 4, min = 1
    const random = Math.round(Math.random() * (max - min) + min)
    return (
        <ScrollView>
            <Txt>{t(`dynamic_script_${random}`)}</Txt><Br />

            {/* i18next-parser won't read comment in here. */}
            <Txt>{t('context_list', { context: random })}</Txt><Br />
            <Txt>
                <Trans i18nKey='nesting_trans' t={t}>
                    This key is called: $t(nesting_key).
                </Trans><Br />
                <Trans
                    i18nKey='context_nesting_trans'
                    t={t}
                    values={{ var: random }}
                >
                    You can even pass option in nesting key, for example: $t(context_list, {'{'} "context": "{'{{var}}'}" {'}'}).
                </Trans>
            </Txt>
        </ScrollView >
    )
}