import React from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { useTranslation, Trans } from 'react-i18next'
import { box } from './style'
import { Txt, Br, Code, H } from './mini'

const style = StyleSheet.create({
    simple: { ...box }
})

export default function (props) {
    const { t } = useTranslation('simple')

    /* useTranslation */
    const new_key = 'replacement'

    /* Trans */
    const trans_plural = 100

    return (
        <ScrollView style={style.simple}>
            <H level={1}>{t('plain_script')}</H><Br />
            <Txt>
                {t('plural_script', { count: 0 })} > {t('plural_script', { count: 1 })} > {t('plural_script', { count: 2 })}
            </Txt><Br />
            <Txt>
                {t('context_script', { context: 'Batman' })} vs.
                {t('context_script', { context: 'Superman' })}
            </Txt><Br />
            <Txt>{t('replacement_script', { new_key })}</Txt><Br />
            <Txt>{t('key_script.item1')} > {t('key_script.item2')}</Txt><Br />
            <Txt>
                <Trans i18nKey='trans_script' t={t}>
                    This is the script has been localized by <Code>Trans</Code> tags.
                </Trans>
            </Txt><Br />
            <Txt>
                <Trans i18nKey='plural_trans_script' count={trans_plural} t={t}>
                    This is both plural and replacement <Code>Trans</Code> script with {{ trans_plural }} apple.
                </Trans>
            </Txt><Br />
        </ScrollView>
    )
}