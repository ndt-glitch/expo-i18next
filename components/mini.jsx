import React from 'react'

import { StyleSheet, Text } from 'react-native'

const style = StyleSheet.create({
    txt: {
        color: '#ececec',
        fontSize: 13
    },
    lnk: {
        color: '#b3c430'
    },
    code: {
        backgroundColor: '#ececec',
        color: '#222831',
        fontFamily: 'monospace',
        fontSize: 12,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#30475e',
        borderRadius: 2
    }
})

const heading = [
    StyleSheet.create({ fontSize: 25 }),
    StyleSheet.create({ fontSize: 22 }),
    StyleSheet.create({ fontSize: 18 })
]

export function Br() {
    return <Text>{'\n'}</Text>
}

export function Txt(props) {
    return <Text style={style.txt}>{props.children}</Text>
}

export function H(props) {
    const { level } = props
    if (level > 0 && level < 4)
        return <Text style={heading[level - 1]}>{props.children}</Text>
    else
        throw new SyntaxError('The level of <H> need to be larger than 0 and smaller than 4')
}

export function Lnk(props) {
    const { href } = props
    return <Text onPress={href} style={style.lnk}>{props.children}</Text>
}

export function Code(props) {
    return <Text style={style.code}>{props.children}</Text>
}