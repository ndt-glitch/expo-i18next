/**
 * Tool: i18next-parser
 * Translation script generator configuration
 * @link https://github.com/i18next/i18next-parser#options
 */
module.exports = {
    locales: ['en', 'ja', 'ru', 'vi'],
    output: 'locales/$LOCALE/$NAMESPACE.json',
    input: ['components/*.jsx'],
    defaultNamespace: 'common',
    verbose: true,
    sort: true
}