import React from 'react'
import { StyleSheet, View } from 'react-native'
import Simple from './components/simple'
import Complex from './components/complex'

import './locales'

const style = StyleSheet.create({
    body: {
        backgroundColor: '#30475e',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 14,
    }
})

export default function App() {
    return (
        <View style={style.body}>
            <Simple></Simple>
            <Complex></Complex>
        </View>
    )
}